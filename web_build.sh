#!/bin/bash

set -e

cd "$(dirname "$0")"

OUT_DIR=/tmp/web_build
PUBLIC_DIR="${OUT_DIR}/public"
TARGET=wasm32-unknown-unknown

PROFILE="debug"
if [[ $* == *--release* ]]; then
    PROFILE="release"
fi

RUSTFLAGS=--cfg=web_sys_unstable_apis \
    cargo build --target "${TARGET}" --no-default-features --target-dir "${OUT_DIR}" $@

wasm-bindgen --out-dir "${PUBLIC_DIR}" --web "${OUT_DIR}/${TARGET}/${PROFILE}"/wgpu-egui-starter.wasm
cp ./run.html "${PUBLIC_DIR}/index.html"

python -m http.server 9348 --directory "${PUBLIC_DIR}"
