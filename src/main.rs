use instant::Instant;
use log::{error, info};
use winit::event_loop::{ControlFlow, EventLoop, EventLoopProxy};
use winit::window::Window;

#[macro_use]
pub mod wgpu_util;

#[cfg(not(target_arch = "wasm32"))]
pub const OUTPUT_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Bgra8UnormSrgb;
#[cfg(target_arch = "wasm32")]
pub const OUTPUT_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Bgra8Unorm;

struct EventLoopRepaintSignal(std::sync::Mutex<EventLoopProxy<AppEvent>>);

impl epi::RepaintSignal for EventLoopRepaintSignal {
    fn request_repaint(&self) {
        let _ = self.0.lock().unwrap().send_event(AppEvent::RepaintSignal);
    }
}

// HACK: deal with the lack of Send in EventLoopProxy on wasm32.
#[cfg(target_arch = "wasm32")]
unsafe impl Sync for EventLoopRepaintSignal {}
#[cfg(target_arch = "wasm32")]
unsafe impl Send for EventLoopRepaintSignal {}

pub enum AppEvent {
    RepaintSignal,
    Error(anyhow::Error),
}

impl std::fmt::Debug for AppEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::RepaintSignal => write!(f, "RepaintSignal"),
            Self::Error(arg0) => f.debug_tuple("Error").field(arg0).finish(),
        }
    }
}

#[derive(Default)]
pub struct App {
    errors: Vec<(u32, anyhow::Error)>,
    next_error_id: u32,
}

impl App {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn show_error(&mut self, e: anyhow::Error) {
        self.errors.push((self.next_error_id, e));
        self.next_error_id += 1;
    }

    pub fn update(
        &mut self,
        ctx: &egui::CtxRef,
        frame: &mut epi::Frame<'_>,
        _event_loop: &EventLoopProxy<AppEvent>,
    ) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                egui::menu::menu(ui, "File", |ui| {
                    if ui.button("Open").clicked() {}
                    if ui.button("Quit").clicked() {
                        frame.quit();
                    }
                });
            });
        });

        self.errors.retain(|(error_id, e)| {
            let mut opened = true;
            let mut close_requested = false;
            egui::Window::new("Error")
                .id(egui::Id::new("error popup").with(*error_id))
                .open(&mut opened)
                .collapsible(false)
                .resizable(false)
                .show(ctx, |ui| {
                    let error_str = format!("{:?}", e);
                    ui.label(&error_str);
                    ui.horizontal(|ui| {
                        if ui.button("Copy To Clipboard").clicked() {
                            ui.output().copied_text = error_str;
                        }
                        if ui.button("Close").clicked() {
                            close_requested = true;
                        }
                    });
                });
            opened && !close_requested
        });
    }

    /// This function is called after `update` to take care of work that has to be done with the
    /// `egui_wgpu_backend::RenderPass`, such as updating egui textures. The `RenderPass` is
    /// normally borrowed by the `epi::Frame`.
    pub fn update_wgpu(
        &mut self,
        _device: &wgpu::Device,
        _queue: &wgpu::Queue,
        _rpass: &mut egui_wgpu_backend::RenderPass,
    ) {
    }
}

async fn app_main(window: Window, event_loop: EventLoop<AppEvent>) {
    let backend = wgpu::Backends::PRIMARY;
    let instance = wgpu::Instance::new(backend);
    let surface = unsafe { instance.create_surface(&window) };

    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            compatible_surface: Some(&surface),
            force_fallback_adapter: false,
        })
        .await
        .expect("failed to get wgpu adapter");

    let adapter_info = adapter.get_info();
    info!("Adapter: {}", adapter_info.name);
    info!("Vendor : 0x{:x}", adapter_info.vendor);
    info!("Device : 0x{:x}", adapter_info.device);
    info!("Type   : {:?}", adapter_info.device_type);
    info!("Backend: {:?}", adapter_info.backend);

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                features: wgpu::Features::default(),
                limits: wgpu::Limits::default(),
                label: None,
            },
            None,
        )
        .await
        .expect("failed to create wgpu device");

    device.on_uncaptured_error(|error| {
        error!("uncaptured device error: {}", error);
        panic!("wgpu error")
    });

    let size = window.inner_size();
    let mut sc_desc = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: OUTPUT_FORMAT,
        width: size.width as u32,
        height: size.height as u32,
        present_mode: wgpu::PresentMode::Mailbox,
    };
    surface.configure(&device, &sc_desc);

    let repaint_signal = std::sync::Arc::new(EventLoopRepaintSignal(std::sync::Mutex::new(
        event_loop.create_proxy(),
    )));

    // We use the egui_winit_platform crate as the platform.
    let mut platform =
        egui_winit_platform::Platform::new(egui_winit_platform::PlatformDescriptor {
            physical_width: size.width as u32,
            physical_height: size.height as u32,
            scale_factor: window.scale_factor(),
            font_definitions: egui::FontDefinitions::default(),
            style: Default::default(),
        });

    // We use the egui_wgpu_backend crate as the render backend.
    let mut egui_rpass = egui_wgpu_backend::RenderPass::new(&device, OUTPUT_FORMAT, 1);

    let mut app = App::new();

    let event_loop_proxy = event_loop.create_proxy();
    let start_time = Instant::now();
    event_loop.run(move |event, _, control_flow| {
        platform.handle_event(&event);

        match event {
            winit::event::Event::RedrawRequested(..) => {
                platform.update_time(start_time.elapsed().as_secs_f64());

                let output_frame = match surface.get_current_texture() {
                    Ok(frame) => frame,
                    Err(e) => {
                        error!("Dropped frame with error: {}", e);
                        return;
                    }
                };

                platform.begin_frame();
                let mut app_output = epi::backend::AppOutput::default();

                let mut frame = epi::backend::FrameBuilder {
                    info: epi::IntegrationInfo {
                        web_info: None,
                        cpu_usage: None,
                        seconds_since_midnight: None,
                        native_pixels_per_point: Some(window.scale_factor() as _),
                        prefer_dark_mode: None,
                    },
                    tex_allocator: &mut egui_rpass,
                    output: &mut app_output,
                    repaint_signal: repaint_signal.clone(),
                }
                .build();

                let ctx = &platform.context();

                app.update(ctx, &mut frame, &event_loop_proxy);
                app.update_wgpu(&device, &queue, &mut egui_rpass);

                let (output, paint_commands) = platform.end_frame(None);
                if output.needs_repaint {
                    window.request_redraw();
                }

                let paint_jobs = platform.context().tessellate(paint_commands);

                let mut encoder =
                    device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

                // Upload all resources for the GPU.
                let screen_descriptor = egui_wgpu_backend::ScreenDescriptor {
                    physical_width: sc_desc.width,
                    physical_height: sc_desc.height,
                    scale_factor: window.scale_factor() as f32,
                };
                egui_rpass.update_texture(&device, &queue, &platform.context().texture());
                egui_rpass.update_user_textures(&device, &queue);
                egui_rpass.update_buffers(&device, &queue, &paint_jobs, &screen_descriptor);

                // Record all render passes.
                egui_rpass
                    .execute(
                        &mut encoder,
                        &output_frame.texture.create_view(&Default::default()),
                        &paint_jobs,
                        &screen_descriptor,
                        Some(wgpu_util::BLACK.into()),
                    )
                    .expect("failed to execute egui rendering pass");

                queue.submit(std::iter::once(encoder.finish()));
                output_frame.present();

                *control_flow = if app_output.quit {
                    ControlFlow::Exit
                } else if output.needs_repaint {
                    ControlFlow::Poll
                } else {
                    ControlFlow::Wait
                }
            }
            winit::event::Event::UserEvent(AppEvent::RepaintSignal) => {
                window.request_redraw();
            }
            winit::event::Event::UserEvent(AppEvent::Error(e)) => {
                app.show_error(e);
            }
            winit::event::Event::WindowEvent { event, .. } => match event {
                winit::event::WindowEvent::Resized(size) => {
                    if size.width > 0 && size.height > 0 {
                        sc_desc.width = size.width;
                        sc_desc.height = size.height;

                        surface.configure(&device, &sc_desc);
                    }
                }
                winit::event::WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                }
                _ => {
                    window.request_redraw();
                }
            },
            winit::event::Event::MainEventsCleared => {
                // On some platforms, the use of window.request_redraw() is not enough, we have to
                // enable the polling controlflow and request a redraw every loop.
                if *control_flow == ControlFlow::Poll {
                    window.request_redraw();
                }
            }
            _ => {}
        }
    });
}

#[cfg(not(target_arch = "wasm32"))]
fn main() {
    env_logger::Builder::from_default_env()
        .format_module_path(false)
        .format_timestamp(None)
        .filter(Some("wgpu_egui_starter"), log::LevelFilter::Info)
        .init();

    let event_loop: EventLoop<AppEvent> = EventLoop::with_user_event();
    let window = winit::window::WindowBuilder::new()
        .with_decorations(true)
        .with_resizable(true)
        .with_transparent(false)
        .with_title(format!(
            "{}-{}",
            env!("CARGO_PKG_NAME"),
            env!("CARGO_PKG_VERSION")
        ))
        .with_inner_size(winit::dpi::PhysicalSize {
            width: 1280,
            height: 1024,
        })
        .build(&event_loop)
        .expect("failed to create window");

    futures::executor::block_on(app_main(window, event_loop));
}

#[cfg(target_arch = "wasm32")]
pub fn main() -> Result<(), wasm_bindgen::JsValue> {
    use winit::platform::web::WindowExtWebSys;

    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("could not initialize logger");

    let event_loop = EventLoop::with_user_event();
    let window = winit::window::WindowBuilder::new()
        .with_resizable(false)
        .with_inner_size(winit::dpi::PhysicalSize::<u32>::new(1024, 576))
        .build(&event_loop)
        .unwrap();

    // On wasm, append the canvas to the document body
    web_sys::window()
        .and_then(|win| win.document())
        .and_then(|doc| doc.body())
        .and_then(|body| {
            body.append_child(&web_sys::Element::from(window.canvas()))
                .ok()
        })
        .expect("couldn't append canvas to document body");

    wasm_bindgen_futures::spawn_local(app_main(window, event_loop));

    Ok(())
}
