use std::path::Path;

use wgpu::*;
pub struct ThemeColor([u8; 3]);

impl From<ThemeColor> for Color {
    fn from(color: ThemeColor) -> Self {
        srgb_to_color(color.0[0], color.0[1], color.0[2])
    }
}

impl From<ThemeColor> for [f32; 4] {
    fn from(color: ThemeColor) -> Self {
        srgb_to_linear(color.0[0], color.0[1], color.0[2])
    }
}

impl From<ThemeColor> for [u8; 4] {
    fn from(color: ThemeColor) -> Self {
        let c: [f32; 4] = color.into();

        [
            (c[0] * 255.0) as u8,
            (c[1] * 255.0) as u8,
            (c[2] * 255.0) as u8,
            255,
        ]
    }
}

pub const BLACK: ThemeColor = ThemeColor([0x1C, 0x1B, 0x19]);
pub const RED: ThemeColor = ThemeColor([0xFF, 0x31, 0x28]);
pub const GREEN: ThemeColor = ThemeColor([0x51, 0x9F, 0x50]);
pub const YELLOW: ThemeColor = ThemeColor([0xFB, 0xB8, 0x29]);
pub const BLUE: ThemeColor = ThemeColor([0x55, 0x73, 0xA3]);
pub const PURPLE: ThemeColor = ThemeColor([0xE0, 0x2C, 0x6D]);
pub const CYAN: ThemeColor = ThemeColor([0x0A, 0xAE, 0xB3]);
pub const WHITE: ThemeColor = ThemeColor([0x91, 0x81, 0x75]);
pub const BRIGHT_BLACK: ThemeColor = ThemeColor([0x2D, 0x2B, 0x28]);
pub const BRIGHT_RED: ThemeColor = ThemeColor([0xF7, 0x53, 0x41]);
pub const BRIGHT_GREEN: ThemeColor = ThemeColor([0x98, 0xBC, 0x37]);
pub const BRIGHT_YELLOW: ThemeColor = ThemeColor([0xFE, 0xD0, 0x6E]);
pub const BRIGHT_BLUE: ThemeColor = ThemeColor([0x8E, 0xB2, 0xF7]);
pub const BRIGHT_PURPLE: ThemeColor = ThemeColor([0xE3, 0x56, 0x82]);
pub const BRIGHT_CYAN: ThemeColor = ThemeColor([0x53, 0xFD, 0xE9]);
pub const BRIGHT_WHITE: ThemeColor = ThemeColor([0xFC, 0xE8, 0xC3]);
pub const FOREGROUND: ThemeColor = ThemeColor([0xeb, 0xdb, 0xb2]);
pub const BACKGROUND: ThemeColor = ThemeColor([0x28, 0x28, 0x28]);
pub const CURSORCOLOR: ThemeColor = ThemeColor([0xeb, 0xdb, 0xb2]);

pub fn srgb_to_linear(r: u8, g: u8, b: u8) -> [f32; 4] {
    [
        (r as f32 / 255.0).powf(2.2),
        (g as f32 / 255.0).powf(2.2),
        (b as f32 / 255.0).powf(2.2),
        1.0,
    ]
}

pub fn srgb_to_color(r: u8, g: u8, b: u8) -> Color {
    Color {
        r: (r as f64 / 255.0).powf(2.2),
        g: (g as f64 / 255.0).powf(2.2),
        b: (b as f64 / 255.0).powf(2.2),
        a: 1.0,
    }
}

#[macro_export]
macro_rules! load_shader_file {
    ($device:ident, $path:literal) => {{
        #[cfg(any(target_arch = "wasm32", not(debug_assertions)))]
        {
            $crate::wgpu_util::create_shader_module($device, include_str!(concat!("../", $path)))
        }
        #[cfg(not(any(target_arch = "wasm32", not(debug_assertions))))]
        {
            $crate::wgpu_util::create_shader_module_from_file($device, $path)
        }
    }};
}

pub fn create_shader_module_from_file(
    device: &Device,
    wgsl_path: impl AsRef<Path>,
) -> ShaderModule {
    device.create_shader_module(&ShaderModuleDescriptor {
        label: None,
        source: ShaderSource::Wgsl(
            std::fs::read_to_string(wgsl_path)
                .expect("failed to read source wgsl")
                .into(),
        ),
    })
}

pub fn create_shader_module(device: &Device, source_wgsl: &str) -> ShaderModule {
    device.create_shader_module(&ShaderModuleDescriptor {
        label: None,
        source: ShaderSource::Wgsl(source_wgsl.into()),
    })
}

pub fn create_output_texture(device: &Device, width: u32, height: u32) -> Texture {
    device.create_texture(&TextureDescriptor {
        label: None,
        size: Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: TextureFormat::Bgra8Unorm,
        usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
    })
}
